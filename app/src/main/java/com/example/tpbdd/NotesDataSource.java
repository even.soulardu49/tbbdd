package com.example.tpbdd;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class NotesDataSource {

  private SQLiteDatabase database;
  private MySQLiteHelper dbHelper;
  private String[] allColumns = {
      MySQLiteHelper.COLUMN_ID,
      MySQLiteHelper.COLUMN_PRENOM,
      MySQLiteHelper.COLUMN_NOTESVT,
      MySQLiteHelper.COLUMN_NOTEINFO,
      MySQLiteHelper.COLUMN_NOTEMATH
  };

  public NotesDataSource(Context context) {
    dbHelper = new MySQLiteHelper(context);
  }

  public void open() throws SQLException {
    database = dbHelper.getWritableDatabase();
  }

  public void close() {
    dbHelper.close();
  }

  public Notes createNotes(String prenom, double noteSVT, double noteInfo, double noteMath) {
    ContentValues values = new ContentValues();
    values.put(MySQLiteHelper.COLUMN_PRENOM, prenom);
    values.put(MySQLiteHelper.COLUMN_NOTESVT, noteSVT);
    values.put(MySQLiteHelper.COLUMN_NOTEINFO, noteInfo);
    values.put(MySQLiteHelper.COLUMN_NOTEMATH, noteMath);

    long insertId = database.insert(MySQLiteHelper.TABLE_NAME, null,
        values);
    Cursor cursor = database.query(MySQLiteHelper.TABLE_NAME,
        allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
        null, null, null);
    cursor.moveToFirst();
    Notes newNotes = cursorToNotes(cursor);
    cursor.close();
    return newNotes;
  }

  public List<Notes> getAllNotes() {
    List<Notes> notes = new ArrayList<Notes>();

    Cursor cursor = database.query(MySQLiteHelper.TABLE_NAME,
        allColumns, null, null, null, null, null);

    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
      Notes note = cursorToNotes(cursor);
      notes.add(note);
      cursor.moveToNext();
    }

    cursor.close();
    return notes;
  }

  public void deleteNote(Notes note) {
    long id = note.getId();
    System.out.println("Suppression des notes avec l'ID : " + id);
    database.delete(MySQLiteHelper.TABLE_NAME, MySQLiteHelper.COLUMN_ID
        + " = " + id, null);
  }

  private Notes cursorToNotes(Cursor cursor) {
    Notes notes = new Notes();
    notes.setId(cursor.getLong(0));
    notes.setPrenom(cursor.getString(1));
    notes.setNoteSVT(cursor.getDouble(2));
    notes.setNoteInfo(cursor.getDouble(3));
    notes.setNoteMath(cursor.getDouble(4));
    return notes;
  }
}
