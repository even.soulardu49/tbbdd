package com.example.tpbdd.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.SimpleAdapter;
import com.example.tpbdd.Notes;
import com.example.tpbdd.NotesDataSource;
import com.example.tpbdd.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DetailActivity extends ListActivity {
  private NotesDataSource datasource;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail);
    datasource = new NotesDataSource(this);
    datasource.open();
    List<Notes> values = datasource.getAllNotes();

    ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String,String>>();

    for(int compteur=0; compteur<values.size(); compteur++){

      HashMap<String,String> temp = new HashMap<String,String>();
      temp.put("prenom",values.get(compteur).getPrenom());
      temp.put("note_svt", Double.toString(values.get(compteur).getNoteSVT()));
      temp.put("note_info", Double.toString(values.get(compteur).getNoteInfo()));
      temp.put("note_math", Double.toString(values.get(compteur).getNoteMath()));

      list.add(temp);
    }

    setContentView(R.layout.activity_detail);
    SimpleAdapter adapter = new SimpleAdapter(
        this,
        list,
        R.layout.listview_perso,
        new String[] {"prenom", "note_svt", "note_info", "note_math"},
        new int[] {R.id.nameTextView, R.id.noteSVTTextView, R.id.noteInfoTextView, R.id.noteMathTextView}
    );

    setListAdapter(adapter);

  }
}
