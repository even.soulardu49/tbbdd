package com.example.tpbdd.activity;

import android.content.Intent;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.tpbdd.InputFilterMinMax;
import com.example.tpbdd.NotesDataSource;
import com.example.tpbdd.R;

public class CreateActivity extends AppCompatActivity {
  private NotesDataSource datasource;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_create);

    final EditText name = findViewById(R.id.editPersonName);
    final EditText noteSVT = findViewById(R.id.editNoteSVT);
    final EditText noteInfo = findViewById(R.id.editNoteInfo);
    final EditText noteMath = findViewById(R.id.editNoteMath);


    final Button saveBtn = findViewById(R.id.saveButton);
    datasource = new NotesDataSource(this);

    name.setFilters(
        new InputFilter[]{ new InputFilterMinMax("0", "20")}
    );
    noteSVT.setFilters(
        new InputFilter[]{ new InputFilterMinMax("0", "20")}
    );
    noteInfo.setFilters(
        new InputFilter[]{ new InputFilterMinMax("0", "20")}
    );
    noteMath.setFilters(
        new InputFilter[]{ new InputFilterMinMax("0", "20")}
    );

    saveBtn.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        datasource.open();
        datasource.createNotes(
            name.getText().toString(),
            Double.parseDouble(noteSVT.getText().toString()),
            Double.parseDouble(noteInfo.getText().toString()),
            Double.parseDouble(noteMath.getText().toString())
        );
        datasource.close();

        Intent myIntent = new Intent(CreateActivity.this, DetailActivity.class);
        startActivity(myIntent);
      }
    });
  }
}