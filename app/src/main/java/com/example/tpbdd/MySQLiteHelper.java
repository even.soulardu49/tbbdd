package com.example.tpbdd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

  public static final String TABLE_NAME = "notes";
  public static final String COLUMN_ID = "_id";
  public static final String COLUMN_PRENOM = "prenom";
  public static final String COLUMN_NOTESVT = "note_svt";
  public static final String COLUMN_NOTEINFO = "note_info";
  public static final String COLUMN_NOTEMATH = "note_math";

  private static final String DATABASE_NAME = "epsinotes4.db";
  private static final int DATABASE_VERSION = 1;

  private static final String DATABASE_CREATE = "create table "
      + TABLE_NAME + "("
      + COLUMN_ID + " integer primary key autoincrement, "
      + COLUMN_PRENOM + " text not null, "
      + COLUMN_NOTESVT + " float not null, "
      + COLUMN_NOTEINFO + " float not null, "
      + COLUMN_NOTEMATH + " float not null " + ");"
      ;

  public MySQLiteHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase database) {
    database.execSQL(DATABASE_CREATE);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    Log.w(MySQLiteHelper.class.getName(),
        "MàJ de la version " + oldVersion + " vers "
            + newVersion + ", les vieilles données ont été supprimées");
    db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    onCreate(db);
  }
}