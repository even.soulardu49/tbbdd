package com.example.tpbdd;

public class Notes {
  private long id;
  private String prenom;
  private double noteSVT;
  private double noteInfo;
  private double noteMath;


  public void setId(long id) {
    this.id = id;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public String toString() {
    return prenom;
  }

  public long getId() {
    return id;
  }

  public String getPrenom() {
    return prenom;
  }

  public double getNoteSVT() {
    return noteSVT;
  }

  public void setNoteSVT(double noteSVT) {
    this.noteSVT = noteSVT;
  }

  public double getNoteInfo() {
    return noteInfo;
  }

  public void setNoteInfo(double noteInfo) {
    this.noteInfo = noteInfo;
  }

  public double getNoteMath() {
    return noteMath;
  }

  public void setNoteMath(double noteMath) {
    this.noteMath = noteMath;
  }
}